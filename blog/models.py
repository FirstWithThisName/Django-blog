from django.db import models
import datetime

# Create your models here.

class Post(models.Model):
	title = models.CharField(max_length=64)
	text = models.CharField(max_length=256)
	created = models.DateTimeField()

	def save(self):
		if self.created == None:
			self.created = datetime.datetime.now()
		super(Post, self).save()

	def __str__(self):
		return self.title
