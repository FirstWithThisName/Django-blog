from django.urls import path

from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('createPost', views.create_post, name='createPost'),
	path(r'show_post', views.show_post, name='show_post'),
]
