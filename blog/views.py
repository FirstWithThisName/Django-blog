from django.shortcuts import render
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from blog.models import Post

import math

# Create your views here.

# shwos all posts.
def index(requests):
	#print(Post.objects.all())
	return HttpResponse(render_to_string('blog_home.html', {"post_list" : Post.objects.all()}))

# save new posts
@csrf_exempt
def create_post(request):
	if request.method == "POST":
		entry = Post(title=request.POST.get("title"), text=request.POST.get("text"))
		entry.save()

	return HttpResponse(render_to_string("createPost.html"))

def show_post(request):
	id = request.GET.get("id")
	post = None
	if id is not None:
		try:
			post = Post.objects.get(pk=id)
		except Post.DoesNotExist as e:
			post = None
		except ValueError as e:
			pass

	return HttpResponse(render_to_string("show_post.html", {"post" : post}))
