# Django-blog

My first project with django.

### Setup
You need a version of django for python3 to run the project.

`pip3 install django `  
`python3 manage.py migrate --run-syncdb`

Start the project with:  
`python3 manage.py migrate`

If you have any error use:  
`pip3 install --upgrade django`  
and try again.
