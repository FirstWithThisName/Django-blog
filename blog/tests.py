from django.test import TestCase
from blog.models import Post

import datetime

# Create your tests here.

class PostTestCase(TestCase):
    
    testPost = None

    def setUp(self):
        self.testPost = Post(title="Unit Test Post", text="content of the unit test")
        self.testPost.save()

    def test_save_time(self):
        self.assertIsNotNone(self.testPost)
        self.assertIsNotNone(self.testPost.created)
        self.assertIsNotNone(self.testPost.title)
        self.assertIsNotNone(self.testPost.text)

    def tearDown(self):
        self.testPost.delete()    
